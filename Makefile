include $(TOPDIR)/rules.mk

PKG_NAME:=mqtt_blackbox_exporter
PKG_VERSION:=0.4.0
PKG_RELEASE:=1

PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://codeload.github.com/inovex/mqtt_blackbox_exporter/tar.gz/v${PKG_VERSION}?
PKG_HASH:=f81dd7e45095a4ea97ac4f35cf97074d3d6931a247aa48ee9041328bc8406f7a

PKG_LICENSE:=Apache-2.0
PKG_LICENSE_FILES:=LICENSE
PKG_MAINTAINER:=levenson@mmer.org

PKG_BUILD_DEPENDS:=golang/host
PKG_BUILD_PARALLEL:=1

GO_PKG:=github.com/inovex/mqtt_blackbox_exporter

include $(INCLUDE_DIR)/package.mk
include ../../lang/golang/golang-package.mk

define Package/mqtt_blackbox_exporter/Default
  TITLE:=Prometheus Exporter for MQTT monitoring
  USERID:=prometheus=112:prometheus=112
  URL:=http://prometheus.io
  DEPENDS:=$(GO_ARCH_DEPENDS)
endef

define Package/mqtt_blackbox_exporter
$(call Package/mqtt_blackbox_exporter/Default)
  SECTION:=utils
  CATEGORY:=Utilities
endef

define Package/mqtt_blackbox_exporter/description
    Prometheus Exporter for MQTT monitoring
endef

define Package/mqtt_blackbox_exporter/install
	$(call GoPackage/Package/Install/Bin,$(1))
	$(CP) ./files/* $(1)/
endef

define Package/mqtt_blackbox_exporter/conffiles
/etc/mqtt_blackbox_exporter/config.yaml
endef

$(eval $(call GoBinPackage,mqtt_blackbox_exporter))
$(eval $(call BuildPackage,mqtt_blackbox_exporter))
